FROM busybox
#FROM alpine
MAINTAINER "Reboot Shen<reboot.shen@gmail.com>"
ADD jenkins272-clean.tar.gz /
ADD artifactory-full.tar.gz /
ADD sonatype-work.tar.gz /
ADD mysql-data.tar.gz /
ADD git.tar.gz /
#COPY mysql-data.tar.gz /backup/
VOLUME ["/var/lib/jenkins","/var/opt/jfrog/artifactory","/sonatype-work","/var/lib/mysql","/git","/svn","/logs","/backup","/etc/puppet/modules","/etc/puppet/manifests"]
CMD ["true"]